package org.example.Singleton;

public class Application {
    private static Application application;
    private static String applicationName = "MyApp1";

    public static Application getApplication() {
        if (application == null) application = new Application();
        return application;
    }

    private Application() {}

    public void setApplicationName(String name) {
        applicationName = name;
    }

    public void showAppName() {
        System.out.println(applicationName);
    }
}
