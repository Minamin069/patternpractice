package org.example.Prototype;

public interface Copyable {
    Object copy();
}
