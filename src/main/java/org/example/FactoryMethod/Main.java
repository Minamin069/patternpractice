package org.example.FactoryMethod;


public class Main {
    public static void main(String[] args) {
        DeveloperFactory developerFactory = createDeveloperBySpecialty("C++");
        Developer developer = developerFactory.createDeveloper();

        developer.writeCode();
    }

    static DeveloperFactory createDeveloperBySpecialty(String specialty) {
        if(specialty.equals("Java")) {
            return new JavaDeveloperFactory();
        } else if (specialty.equals("C++")) {
            return new CppDeveloperFactory();
        } else {
            throw new RuntimeException(specialty + " is unknown specialty!");
        }
    }
}