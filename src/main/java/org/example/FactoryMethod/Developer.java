package org.example.FactoryMethod;

public interface Developer {
    void writeCode();
}
