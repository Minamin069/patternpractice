package org.example.FactoryMethod;

public interface DeveloperFactory {
    Developer createDeveloper();
}
