package org.example.Builder;

public class DiplomeMobileApp extends MobileAppBuilder{
    @Override
    void buildName() {
        mobileApp.setName("D-Pro");
    }

    @Override
    void buildTech() {
        mobileApp.setTech(Tech.KOTLIN);
    }

    @Override
    void buildPrice() {
        mobileApp.setPrice(150000);
    }
}
