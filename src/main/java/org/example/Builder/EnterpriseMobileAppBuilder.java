package org.example.Builder;

public class EnterpriseMobileAppBuilder extends MobileAppBuilder{
    @Override
    void buildName() {
        mobileApp.setName("Коммерческий проект мобильного приложения");
    }

    @Override
    void buildTech() {
        mobileApp.setTech(Tech.FLUTTER);
    }

    @Override
    void buildPrice() {
        mobileApp.setPrice(300000);
    }
}
