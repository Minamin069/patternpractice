package org.example.Builder;

public enum Tech {
    JAVA, KOTLIN, REACT_NATIVE, FLUTTER, XAMARIN
}
