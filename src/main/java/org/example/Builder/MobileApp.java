package org.example.Builder;

public class MobileApp {
    private String name;
    private Tech tech;
    private Integer price;

    public void setName(String name) {
        this.name = name;
    }

    public void setTech(Tech tech) {
        this.tech = tech;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "name: " + name + '\n' +
                "tech: " + tech + '\n' +
                "price: " + price;
    }
}
