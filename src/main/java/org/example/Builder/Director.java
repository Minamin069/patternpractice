package org.example.Builder;

public class Director {
    private MobileAppBuilder builder;

    public void setBuilder(MobileAppBuilder builder) {
        this.builder = builder;
    }

    MobileApp buildMobileApp() {
        builder.createMobileApp();
        builder.buildName();
        builder.buildTech();
        builder.buildPrice();

        MobileApp mobileApp = builder.getMobileApp();

        return mobileApp;
    }
}
