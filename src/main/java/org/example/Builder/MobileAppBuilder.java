package org.example.Builder;

public abstract class MobileAppBuilder {
    MobileApp mobileApp;

    void createMobileApp() {
        mobileApp = new MobileApp();
    }

    abstract void buildName();
    abstract void buildTech();
    abstract void buildPrice();

    MobileApp getMobileApp() {
        return mobileApp;
    }
}
