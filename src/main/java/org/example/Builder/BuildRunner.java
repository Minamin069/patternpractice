package org.example.Builder;

public class BuildRunner {
    public static void main(String[] args) {
        Director director = new Director();
        director.setBuilder(new EnterpriseMobileAppBuilder());

        MobileApp mobileApp = director.buildMobileApp();

        System.out.println(mobileApp);
    }
}
