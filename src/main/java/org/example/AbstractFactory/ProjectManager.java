package org.example.AbstractFactory;

public interface ProjectManager {
    void manageProject();
}
