package org.example.AbstractFactory;

import org.example.AbstractFactory.banking.BankingTeamFactory;
import org.example.AbstractFactory.mobile.MobileTeamFactory;

public class MobileProject {
    public static void main(String[] args) {
        ProjectTeamFactory mobileTeamFactory = new MobileTeamFactory();
        Developer developer = mobileTeamFactory.getDeveloper();
        Tester tester = mobileTeamFactory.getTester();
        ProjectManager projectManager = mobileTeamFactory.getProjectManager();

        developer.writeCode();
        tester.testCode();
        projectManager.manageProject();

        System.out.println("---------------------------------");

        ProjectTeamFactory bankingTeamFactory = new BankingTeamFactory();
        Developer developer1 = bankingTeamFactory.getDeveloper();
        Tester tester1 = bankingTeamFactory.getTester();
        ProjectManager projectManager1 = bankingTeamFactory.getProjectManager();

        developer1.writeCode();
        tester1.testCode();
        projectManager1.manageProject();
    }
}
