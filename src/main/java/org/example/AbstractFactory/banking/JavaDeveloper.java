package org.example.AbstractFactory.banking;

import org.example.AbstractFactory.Developer;

public class JavaDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Пишу код на джяве!!!");
    }
}
