package org.example.AbstractFactory.banking;

import org.example.AbstractFactory.Developer;
import org.example.AbstractFactory.ProjectManager;
import org.example.AbstractFactory.ProjectTeamFactory;
import org.example.AbstractFactory.Tester;

public class BankingTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new JavaDeveloper();
    }

    @Override
    public Tester getTester() {
        return new QATester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new BankingPM();
    }
}
