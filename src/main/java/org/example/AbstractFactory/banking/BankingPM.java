package org.example.AbstractFactory.banking;

import org.example.AbstractFactory.ProjectManager;

public class BankingPM implements ProjectManager {
    @Override
    public void manageProject() {
        System.out.println("Управляю банковым проектом!!!");
    }
}
