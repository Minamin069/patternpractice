package org.example.AbstractFactory.mobile;

import org.example.AbstractFactory.Developer;
import org.example.AbstractFactory.ProjectManager;
import org.example.AbstractFactory.ProjectTeamFactory;
import org.example.AbstractFactory.Tester;

public class MobileTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new KotlinDeveloper();
    }

    @Override
    public Tester getTester() {
        return new ManualTester();
    }

    @Override
    public ProjectManager getProjectManager() {
        return new MobilePM();
    }
}
