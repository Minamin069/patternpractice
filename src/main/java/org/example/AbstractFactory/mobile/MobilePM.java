package org.example.AbstractFactory.mobile;

import org.example.AbstractFactory.ProjectManager;

public class MobilePM implements ProjectManager {
    @Override
    public void manageProject() {
        System.out.println("Управляю проектом мобильного приложения!!!");
    }
}
