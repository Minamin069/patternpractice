package org.example.AbstractFactory;

public interface Developer {
    void writeCode();
}
